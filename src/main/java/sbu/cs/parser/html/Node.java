package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface {

    private String tagName;
    private String inside;
    private Map<String, String> attributes = new HashMap<>();
    private List<Node> children = new ArrayList<>();

    public void setTagName(String tagName) {
        for (int i = 0; i < tagName.length(); i++) {
            if (tagName.charAt(i) != ' ') {
                this.tagName = tagName;
            }
            else{
                String[] attributes = tagName.split(" ");
                for (int j = 1; j < attributes.length; j++) {
                    String[] temp = attributes[j].split("(=|:)");
                    if (temp.length == 2) {
                        setAttributes(temp[0], temp[1]);
                    }
                }
            }
        }

    }

    public void setInside(String inside) {
        this.inside = inside;
    }

    public void setAttributes(String key, String value) {
        attributes.put(key, value);
    }

    public void setChildren(Node children) {
        this.children.add(children);
    }

    /*
    * this function will return all that exists inside a tag
    * for example for <html><body><p>hi</p></body></html>, if we are on
    * html tag this function will return <body><p1>hi</p1></body> and if we are on
    * body tag this function will return <p1>hi</p1> and if we are on
    * p tag this function will return hi
    * if there is nothing inside tag then null will be returned
     */
    @Override
    public String getStringInside() {
        return inside;
    }

    public String getTagName() {
        return tagName;
    }

    /*
    *
     */
    @Override
    public List<Node> getChildren() {
        return children;
    }

    /*
    * in html tags all attributes are in key value shape. this function will get a attribute key
    * and return it's value as String.
    * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key) {
        return attributes.get(key);
    }

    @Override
    public String toString() {
        return "tagName : " + tagName ;
    }
}
