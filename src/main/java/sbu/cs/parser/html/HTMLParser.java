package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.List;

public class HTMLParser {
    private static Node node = new Node();
    private static String inside = null;
    /*
    * this function will get a String and returns a Dom object
     */
    public static Node parse(String document) {
        if (document.equals("<>")) {
            return node;
        }
        document = document.replaceAll("\n", "");
        document = document.replaceAll("\"", "");
        document = document.replaceAll("</br>", "");
        document = document.replaceAll("  ", "");
        int startOfInsideString = 0, finishOfInsideString = 1;
        outer : for (int i = 0; i < document.length(); i++) {
            if (document.charAt(i) == '>') {
                startOfInsideString = i + 1;
                for (int j = document.length() - 1; j > 0; j--) {
                    if (document.charAt(j) == '<') {
                        finishOfInsideString = j;
                        break outer;
                    }
                }
            }
        }
        node.setTagName(document.substring(1, startOfInsideString - 1));
        if (startOfInsideString < finishOfInsideString) {
            document = document.substring(startOfInsideString, finishOfInsideString);
        }
        else {
            document = "";
        }
        node.setInside(document.equals("") ? null : document);
        String[] tags = document.split("><");
        if (tags.length < 2) {
            return node;
        }
        tags[0] = tags[0] + ">";
        tags[tags.length - 1] = "<" + tags[tags.length - 1];
        if (tags.length > 2) {
            for (int i = 1; i < tags.length - 1; i++) {
                tags[i] = "<" + tags[i] + ">";
                System.out.println("tags i = " + tags[i]);
            }
        }
        for (int i = 0; i < tags.length; i++) {
            System.out.println(parse(tags[i]));
            node.setChildren(parse(tags[i]));
        }

        return node;
    }

    /*
    * a function that will return string representation of dom object.
    * only implement this after all other functions been implemented because this
    * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        // TODO implement this for more score
        return null;
    }
}
