package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        data = data.replace("{", "");
        data = data.replaceAll("}$", "");
        data = data.replaceAll(" ", "");
        data = data.replaceAll("\n", "");
        data = data.replaceAll("\t", "");
        for (int i = 0; i < data.length(); i++) {
            if (data.charAt(i) == '[') {
                int j = i + 1;
                do{
                    if (data.charAt(j) == ',') {
                        data = data.substring(0, j) + " " + data.substring(j + 1);
                    }
                    j++;
                }while (data.charAt(j) != ']');
            }
        }
        List<JsonKeyValue> keyValues = new ArrayList<>();
        String[] parts = data.split(",");
        for(String part : parts){
            String[] temp = part.split(":");
            JsonKeyValue jsonKeyValue = new JsonKeyValue(temp[0], temp[1]);
            keyValues.add(jsonKeyValue);
        }
        Json json = new Json(keyValues);
        return json;
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
