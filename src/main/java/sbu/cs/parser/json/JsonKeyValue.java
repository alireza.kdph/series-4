package sbu.cs.parser.json;

public class JsonKeyValue {
    String key;
    String value;

    public JsonKeyValue(String key, String value) {
        key = key.replaceAll("\"", "");
        value = value.replaceAll("\"", "");
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
