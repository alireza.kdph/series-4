package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class Json implements JsonInterface {
    private List<JsonKeyValue> jsonKeyValues = new ArrayList<>();

    public Json(List<JsonKeyValue> jsonKeyValues) {
        this.jsonKeyValues = jsonKeyValues;
    }

    @Override
    public String getStringValue(String key) {

        for (int i = 0; i < jsonKeyValues.size(); i++) {
            if (jsonKeyValues.get(i).getKey().equals(key)) {
                for (int j = 0; j < jsonKeyValues.get(i).getValue().length(); j++) {
                    if(jsonKeyValues.get(i).getValue().charAt(j) == '['){
                        String str = jsonKeyValues.get(i).getValue().replaceAll(" ", ", ");
                        return str;
                    }
                }
                return jsonKeyValues.get(i).getValue();
            }
        }
        return null;
    }
}
